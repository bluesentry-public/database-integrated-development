# Database Integrated Development

Public repo to share Integrated Database code for AWS APN blog


Notes:  
* This setup is configured for a MySQL database so some changes would be required for other databases 
* All Docker invocations are "generic" and do not require a custom Docker file

* Prerequisites:
  * Install latest Liquibase version
  * Install latest Docker version

* Setup instructions
  * Configure GitLab runners
  * Set GitLab variables
  * Copy application.gitlab-ci.yml to .gitlab-ci.yml in your application repository
  * Edit the application .gitlab-ci.yml to change `lbtest` to your specific schema
  * Copy database.gitlab-ci.yml to .gitlab-ci.yml in your database repository
  * Try it!


* How to configure GitLab runners (You will execute these steps for both repositories.  All runners were tested running on same host)
  * Login to GitLab
  * Open your project
  * Is left side-bar menu open Settings->CI/CD
  * Expand the Runners section
  * Install Runner per instructions
  * Configure Runner (You will execute these steps twice to create two (2) Runners for each repository)
    * Execute:  gitlab-runner register
    * Enter value for GitLab instance URL from Runners section
    * Enter registration token from Runners section
    * Set Runner description 
    * Set tags 
      * Runner 1 : shell
      * Runner 2 : mysql
    * Set executor
      * Runner 1 : shell
      * Runner 2 : docker  -> docker version mysql:latest
  * Ensure Runners are now visible in the Runners section of the GibLab repository


* How to set GitLab variables
  * Login to GitLab
  * Open your project
  * Is left side-bar menu open Settings->CI/CD
  * Expand the Variables section
  * Create following variables with your specific values
    * Application Repository
      * DB_HOST:     -- Host name of DB 
      * DB_USER:     -- User name to connect to DB 
      * DB_PASSWORD: -- Password for above user 
    * Database Repository
      * MYSQL_PASSWORD:         -- root password for MySQL Docker container
      * MYSQL_USER:             -- root user for MySQL Docker container (typically root)
      * DB_SCHEMA:              -- Schema for database changes
      * DB_HOST_BRANCH:         -- Host name of DB for developer branch execution
      * DB_USER_BRANCH:         -- User name to connect to for developer branch execution
      * DB_PASSWORD_BRANCH:     -- Password for above user for developer branch execution
      * DB_HOST_MASTER:         -- Host name of DB for master branch execution (integrated development DB)
      * DB_USER_MASTER:         -- User name to connect to DB for master branch execution (integrated development DB)
      * DB_PASSWORD_MASTER:     -- Password for above user for master branch execution (integrated development DB)
    * Unselect "Protect Variable" check box

* Sample Liquibase files included
  * dbchangelog.xml -- Sample XML format Liquibase file containing a couple of example table entries
  * liquibase.properties.mysql  -- Sample liquibase.properties file to give example of required format
    * Change YourDatabaseUserHere to your specific database user
    * Change YourDatabasePasswordHere to your specific database user's password
    * You will need to set `classpath` and `driver` values for your specific installation/configuration
